#!/opt/local/bin/python

import sys

if len(sys.argv) != 2:
	print "Usage: " + sys.argv[0] + " <CSV file>"
	sys.exit(1)

linenum = 1

file = open(sys.argv[1], 'r')
for line in file.readlines():
	if ((linenum % 2) == 0):
		sys.stdout.write('<tr class="grey">')
	else:
		sys.stdout.write('<tr>')
	linenum = linenum + 1

	for token in line.strip().split(','):
		sys.stdout.write('<td>' + token + '</td>')

	sys.stdout.write('</tr>\n')
	
file.close()
sys.exit(0)
